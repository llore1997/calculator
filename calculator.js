const NoCalculatorFunctionNameError = require('./customExceptions');
const Person = require('./person');


var persona1 = new Person ('llorenç','Frances','professor');
var persona2 = new Person ('Marcos','Martinez','alumne');



let fAdd = function(array_) {   
  return array_.reduce((valorAnterior, valorActual) => {
     return valorAnterior - valorActual;
  });
}



let fsubtraction = function(array_) {   
  return array_.reduce((valorAnterior, valorActual) => {
     return valorAnterior - valorActual;
  });
}


let fMultiplier = function(array_) {
  return array_.reduce((valorAnterior, valorActual)=>{
     return valorAnterior * valorActual;
  });
} 


//It doubles every single item of the array
let fDoublefier = function(array_){
  return array_.map(function(valorActual){
    return valorActual*2;
  });
}


let operationObject={
  'add':fAdd,
  'multiplier':fMultiplier,
  'doublefier':fDoublefier,
  'subtraction':fsubtraction
};

let exceptionNotOperands = {'code':1,'message':'Can call calculator with an empty array'};
let exceptionOperationNotFound



function calculator(array_,operation_,person){  
  try {

    if (array_.length<=0) {
    let fOperation=operationObject[operation_];

      
        if (typeof fOperation !== "function"){ 
          throw new NoCalculatorFunctionNameError.NoCalculatorFunctionNameError(); 
          
        }
            
        return operationObject[operation_](array_);
    }
  
   if(operation_ == 'subtraction'){

       if (persona2.rol == 'professor'){
        return operationObject[operation_](array_);

       }else{
        throw new NoCalculatorFunctionNameError.Nopersonpermite(); 
       }
    }

  } catch (error) {    
      console.log(error.name+" "+error.message);
      return error.constructor;
  }
  
}

console.log(calculator([5,6],'subtraction',persona2));
module.exports = calculator;