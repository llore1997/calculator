class NoCalculatorFunctionNameError extends Error {
  constructor() {
    super("No existeix eixa operació");
    this.name = 'NoCalculatorFunctionName';
  }
}


class Nopersonpermite extends Error {
   constructor(){
    super("The person can not perform that operation")
    this.name='Nopersonpermite';

   }

}

module.exports= {NoCalculatorFunctionNameError,Nopersonpermite};
